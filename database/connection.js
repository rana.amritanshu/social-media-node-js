let mongoose = require('mongoose');

let dbInit = () => {
    const MONGOOSE = Symbol.for('mongoose-connection')

    console.log('Connecting to mongo...');
    mongoose.connect(`mongodb://localhost:${process.env.MONGO_PORT}/${process.env.DB_NAME}`, { useNewUrlParser: true });
    global.mongooseConnection = mongoose
    
    global[MONGOOSE] = mongoose

    process.on('SIGINT', () => {
        mongoose.connection.close(() => {
            console.log("Mongoose default connection is disconnected due to application termination");
            process.exit(0);
        });
    });
}

module.exports = {
    dbInit
};