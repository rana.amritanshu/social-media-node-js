const { Map } = require('immutable')

const pagination = Map({
    postsLimit : 10
})

module.exports = pagination