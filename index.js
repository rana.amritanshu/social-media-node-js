require('dotenv').config()
const express = require('express')
const app = express()
require('./database/connection').dbInit()
const { routesInit } = require('./app/routesConnector')

routesInit(app)

app.listen(process.env.PORT, () => {
    console.log(`Listening on port: ${process.env.PORT}`)
})
