const router = require('express').Router()
const Post = require('../controllers/Post')
const PostRepo = require('../repositories/Post')

router.post('/posts', async (req, res) => {
    const user = req[Symbol.for('user-data')]
    const userId = user.id
    let post = new Post()
    let result = await post.insertPost(req, userId)
    let response = {
        success: result.success,
        message: result.message
    }

    if (!!result.success) {
        response.post = result.post
    }

    res.status(result.code || 200).send(response)
})

router.get('/posts', async (req, res) => {
    let page = +req.query.page
    page = page === page ? page : 1
    let posts = new Post()
    let result = await posts.getPosts({ page })
    let response = []
    if (result.code === 200) {
        let data = result.data
        response = { data, meta: { hasMorePages: result.hasMorePages } }
    }

    res.status(result.code || 200).send(response)
})

router.get('/posts/:id', async (req, res) => {
    let postId = req.params.id
    let post = new Post()

    let result = await post.getPost(postId)

    let response = {}
    if (result.code === 200) {
        response = { ...result.data }
    }

    res.status(result.code || 200).send(response)
})

router.put('/posts', async (req, res) => {

})

router.delete('/posts/:id', async (req, res) => {
    
})

module.exports = router;
