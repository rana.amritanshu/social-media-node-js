const router = require('express').Router()
const AuthController = require('../controllers/Auth')
const registerValidation = require('../middlewares/validations/auth/register')

router.post('/register', [registerValidation], async (req, res) => {
    let auth = new AuthController()
    let existingUser = await auth.getUserByEmail(req.body.email)

    if (existingUser) {
        res.status(409).send({
            code: 409,
            message: 'Email already taken'
        })
    } else {
        let response = await auth.insertUser({
            name: req.body.name,
            password: req.body.password,
            email: req.body.email
        })
        res.status(response.status).send(response.data)  
    }
})

router.post('/login', async (req, res) => {
    let auth = new AuthController()
    let existingUser = await auth.getUserByEmail(req.body.email)
    if (existingUser) {
        await auth.comparePassword(req.body.password, existingUser.password)
        let response = auth.login()
        res.status(response.code).send(response.data)
    } else {
        res.status(404).send({
            code: 404,
            message: 'Email is not registered'
        })
    }
})

module.exports = router