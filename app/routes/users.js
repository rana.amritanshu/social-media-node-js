const router = require('express').Router()

router.get('/users', (req, res) => {
    res.send({
        data: [
            {
                id: 12,
                name: 'Amritanshu'
            }
        ]
    })
})

module.exports = router