const mongoose = global[Symbol.for('mongoose-connection')]
const Schema = mongoose.Schema

const userSchema = new Schema({
    name: { type: String, index: true, required: true, trim: true },
    email: { type: String, unique: true, required: true, trim: true },
    password: String,
    isActive: { type: Boolean, default: true },
    lastLogin: { type: Date, default: Date.now() }
})

const Users = mongoose.model('User', userSchema, 'users')

module.exports = Users
