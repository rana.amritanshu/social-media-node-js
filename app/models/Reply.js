const mongoose = global[Symbol.for('mongoose-connection')]
const Schema = mongoose.Schema
const Media = require('./Media')

const commentSchema = new Schema({
    userId: {
        required: true,
        type: Schema.Types.ObjectId,
        index: true
    },
    commentId: {
        required: true,
        type: Schema.Types.ObjectId,
        index: true
    },
    text: {
        required: true,
        type: String,
        trim: true
    },
    media: {type: [Media], default: []},
    likedBy: {type: Array, default: []},
})

const Comments = mongoose.model('Comment', commentSchema, 'comments')

module.exports = Comments
