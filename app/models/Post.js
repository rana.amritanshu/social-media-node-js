const mongoose = global[Symbol.for('mongoose-connection')]
const mongoosePaginate = require('mongoose-paginate')
const Schema = mongoose.Schema
const Media = require('./Media')

const postSchema = new Schema({
    userId: {
        required: true,
        type: Schema.Types.ObjectId,
        index: true
    },
    text: {
        required: true,
        type: String,
        trim: true
    },
    title: {
        required: true,
        type: String,
        trim: true
    },
    media: {type: [Media], default: []},
    comments: {type: Array, default: []}, //comment_id:comment_id {xyz:xyz}
    likedBy: {type: Array, default: []}
})

postSchema.plugin(mongoosePaginate);

const Posts = mongoose.model('Post', postSchema, 'posts')

module.exports = Posts
