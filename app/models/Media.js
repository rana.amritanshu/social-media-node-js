const mongoose = global[Symbol.for('mongoose-connection')]
const Schema = mongoose.Schema

const mediaSchema = new Schema({
    type: {
        required: true,
        type: String,
        enum: ['jpeg', 'mp4', 'png']
    },
    url: {
        required: true,
        type: String
    }
})

module.exports = mediaSchema
