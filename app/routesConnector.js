const bodyParser = require('body-parser')
const AuthRoutes = require('./routes/auth')
const UserRoutes = require('./routes/users')
const PostRoutes = require('./routes/posts')
const {authMiddleware} = require('./middlewares/auth')
const path = require('path')
const ejs = require('ejs')

const routesInit = (app) => {
    app.set('views', path.join(__dirname, '../views'))
    app.set('view engine', 'ejs')
    app.get('/test', (req, res) => {
        res.render('test')
    })

    app.use(bodyParser.json())
    app.use('/api', AuthRoutes)
    app.use(authMiddleware)
    app.use('/api', PostRoutes)
}

module.exports = {
    routesInit
}
