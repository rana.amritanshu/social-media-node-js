const jwt = require('jsonwebtoken')

const authMiddleware = (req, res, next) => {
    let token = req.get('Authorization')
    try {
        token = token.replace('Bearer ', '')
        let userData = jwt.verify(token, process.env.JWT_SALT)
        req[Symbol.for('user-data')] = userData;
    } catch (e) {
        res.status(401).send({
            message: 'Invalid token'
        })
    }
    next()
}

module.exports = {authMiddleware}