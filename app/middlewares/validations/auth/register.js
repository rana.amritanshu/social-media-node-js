const registerSchema = require('../../../validation-schema/auth/register.json')
const router = require('express').Router()
const Ajv = require('ajv')
const Joi = require('@hapi/joi')

const registerAJV = (req, res, next) => {
    let ajv = Ajv()
    let validate = ajv.compile(registerSchema)
    var valid = validate(req.body)
    console.log(registerSchema)
    console.log(valid)
    if (valid) {
        console.log('User data is valid')
    } else {
        console.log('User data is INVALID!')
        console.log(ajv.errors)
    }
    next()
}

const registerJOI = (req, res, next) => {
    const schema = Joi.object().keys({
        name: Joi.string().max(250),
        email: Joi.string().email(),
        password: Joi.string().min(6).max(30)
    }).with('name', 'email').without('password')

    const result = Joi.validate(req.body(), schema)

    console.log(result);
}

module.exports = registerAJV
