const { insertPost, getPosts, getPost } = require('../repositories/Post')
class Post {
    constructor() {

    }

    async insertPost(req, userId) {
        try {
            let post = await insertPost({
                userId: global[Symbol.for('mongoose-connection')].Types.ObjectId(userId),
                text: req.body.text,
                title: req.body.title,
                media: req.body.media
            })
            return {
                success: true,
                post,
                message: 'Post added successfully',
                code: 200
            }
        } catch (e) {
            return {
                success: false,
                message: e,
                code: 500
            }
        }
    }

    async getPosts({ page }) {
        try {
            let posts = await getPosts({page})
            
            if (posts.docs.length === 0) {
                return {
                    code: 404,
                    message: 'No posts found'
                }
            }

            return {
                code: 200,
                data: posts.docs,
                hasMorePages: posts.pages > posts.page
            }
        } catch (e) {
            return {
                code: 500,
                message: 'Internal server error'
            }
        }
    }

    async getPost(postId) {
        try {
            let post = await getPost(postId)

            if (post && post.length === 0) {
                return {
                    code: 404,
                    message: 'No posts found'
                }
            }

            return {
                code: 200,
                data: post
            }
        } catch (e) {
            return {
                code: 500,
                message: 'Internal server error'
            }
        }
    }
}

module.exports = Post