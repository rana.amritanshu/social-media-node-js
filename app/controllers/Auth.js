const { insertUser, getUserByEmail } = require('../repositories/User')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

class Auth {
    constructor(user) {
        this.user = user || {}
        this.doesPasswordMatch = false
        this.user = {}
    }

    generateToken(payload) {
        return jwt.sign(payload, process.env.JWT_SALT)
    }

    async comparePassword(plainPassword, hashedPassword) {
        let doesPasswordMatch = await bcrypt.compare(plainPassword, hashedPassword)
        this.doesPasswordMatch = doesPasswordMatch
        return doesPasswordMatch
    }

    login() {
        if (!!this.doesPasswordMatch) {
            let token = this.generateToken({ email: this.user.email, id: this.user._id })
            return {
                code: 200,
                data: {
                    token,
                    message: 'You have logged in successfully'
                }
            }
        } else {
            return {
                code: 400,
                data: {message: 'Your password is incorrect'}
            }
        }
    }

    async getUserByEmail(email) {
        try {
            let user = await getUserByEmail(email)
            this.user = user
            return user
        } catch (e) {
            return {error: 'error'}
        }
    }

    async insertUser({ name, email, password }) {
        try {
            let user = await insertUser({
                name, email, password
            })
            let token = this.generateToken({ email: user.email, id: user._id })
            return {
                status: 200,
                data: {
                    code: 200,
                    token,
                    message: 'You have registered successfully'
                }
            }
        } catch (e) {
            return {
                data: {
                    message: e.message
                },
                status: 400
            }
        }
    }
}

module.exports = Auth