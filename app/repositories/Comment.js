const Comment = require('../models/Comment')

const insertComment = async ({user_id, text}) => {
    try {
        let comment = new Comment({user_id, text})
        comment = await comment.save()

        return comment
    } catch (e) {
        throw new Error(e.message || 'Something went wrong')
    }
}

module.exports = {
    insertComment
}
