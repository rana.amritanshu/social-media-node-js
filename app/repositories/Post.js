const Post = require('../models/Post')
const paginationConstant = require('../../constants/pagination')

const insertPost = async ({ userId, text, title, media }) => {
    try {
        let post = new Post({ userId, text, media, title })
        post = await post.save()

        return post
    } catch (e) {
        throw new Error(e.message || 'Something went wrong')
    }
}

const getPosts = async ({ page }) => {
    try {
        let options = {
            page,
            limit: paginationConstant.get('postsLimit'),
            select: 'text media'
        };
        let posts = await Post.paginate({}, options)

        return posts
    } catch (e) {
        throw new Error(e.message || e)
    }
}

const getPost = async (postId) => {
    try {
        let ObjectId = global[Symbol.for('mongoose-connection')].Types.ObjectId
        let post = await Post.findOne({'_id': new ObjectId(postId)})
        
        return post._doc
    } catch (e) {
        throw new Error(e.message || e)
    }
}

module.exports = {
    insertPost,
    getPosts,
    getPost
}
