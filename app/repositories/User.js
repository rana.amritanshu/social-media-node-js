const Users = require('../models/User')
const bcrypt = require('bcrypt')

const insertUser = async (req) => {
    try {
        let password = await bcrypt.hash(req.password, 10)
        let user = new Users({name: req.name, email: req.email, password: password})
        user = await user.save()
        return user
    } catch (e) {
        throw new Error(e.message || "Something went wrong")
    }
}

const getUserByEmail = async (email) => {
    try {
        let user = await Users.findOne({email});
        return user
    } catch (e) {
        throw new Error({
            code: 500,
            message: 'Internal server error'
        })
    }
}

module.exports = {
    insertUser,
    getUserByEmail
}